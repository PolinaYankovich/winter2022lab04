//Polina Yankovich 1834306
public class Pizza{
	private double price;
	private String toppin;
	private String crust;
	public void total(){
		System.out.println("Pizza with "+this.crust+" crust would be "+this.price);
	}
	
	public Pizza(String toppin,String crust,double price){
	    this.toppin=toppin;
		this.price=price;
		this.crust=crust;
	}
	
	public void setPrice(double newPrice){
		this.price=newPrice;
	}
	public void setToppin(String newToppin){
		this.toppin=newToppin;
	}
	public void setCrust(String newCrust){
		this.crust=newCrust;
	}
	
	public double getPrice(){
		return this.price;
	}
	public String getToppin(){
		return this.toppin;
	}
	public String getCrust(){
		return this.crust;
	}
}
		